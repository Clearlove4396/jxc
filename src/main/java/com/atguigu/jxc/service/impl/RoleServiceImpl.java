package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.RoleDao;
import com.atguigu.jxc.dao.RoleMenuDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Role;
import com.atguigu.jxc.entity.RoleMenu;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.RoleService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@SuppressWarnings("all")
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private LogService logService;
    @Autowired
    private RoleMenuDao roleMenuDao;

    @Override
    public ServiceVO saveRole(Role role, HttpSession session) {
        User user = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());
        Role roleDB = roleDao.getRoleByRoleIdUserId(role.getRoleId(), user.getUserId());
        // 将用户选择的角色信息放入缓存，以便以后加载权限的时候使用
        session.setAttribute("currentRole", roleDB);
        System.out.println("db the role --->" + roleDB);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> listAll() {
        Map<String,Object> map = new HashMap<>();

        List<Role> roleList = roleDao.findAll();

        logService.save(new Log(Log.SELECT_ACTION, "查询所有角色信息"));

        map.put("rows", roleList);

        return map;
    }

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String roleName) {
        Map<String, Object> map = new HashMap<>();

        // 根据 角色名称 模糊查询 角色列表中角色的数量
        int total = roleDao.getRoleCount(roleName);

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        // 根据 角色名称 模糊分页查询角色列表
        List<Role> roles = roleDao.getRoleList(offSet, rows, roleName);

        logService.save(new Log(Log.SELECT_ACTION, "分页查询角色信息"));

        map.put("total", total);
        map.put("rows", roles);

        return map;
    }

    @Override
    public ServiceVO save(Role role) {

        // 角色ID为空时，说明是新增操作，需要先判断 角色名 是否存在
        if(role.getRoleId() == null) {

            //根据角色名称查询角色
            Role exRole = roleDao.findRoleByName(role.getRoleName());

            //判断角色是否存在
            if(exRole != null) {

                //不为null, 角色存在, 返回10010, "角色已存在".
                return new ServiceVO<>(ErrorCode.ROLE_EXIST_CODE, ErrorCode.ROLE_EXIST_MESS);

            }
            //反之, 为null, 角色不存在, 执行新增操作
            //日志报告记录
            logService.save(new Log(Log.INSERT_ACTION, "新增角色:" + role.getRoleName()));
            //dao层, 操作数据库, 执行新增操作
            roleDao.insertRole(role);

        } else {
            //角色ID不为null, 角色存在, 执行修改操作
            //日志报告记录
            logService.save(new Log(Log.UPDATE_ACTION, "修改角色:"+role.getRoleName()));
            //dao层, 数据库操作, 执行修改操作
            roleDao.updateRole(role);

        }
        //修改成功, 返回 "100", "请求成功"
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    @Override
    public ServiceVO delete(Integer roleId) {
        //根据角色id, 查询属于该角色的用户数量
        int count = roleDao.countUserByRoleId(roleId);
        if (count > 0) {
            return new ServiceVO<>(ErrorCode.ROLE_DEL_ERROR_CODE, ErrorCode.ROLE_DEL_ERROR_MESS);
        }

        // 根据角色id, 删除该角色的所有菜单权限
        roleMenuDao.deleteRoleMenuByRoleId(roleId);

        // roleDao.getRoleById(roleId).getRoleName() -> 根据角色id查询角色, 再获取角色名称
        logService.save(new Log(Log.DELETE_ACTION,"删除角色:"+roleDao.getRoleById(roleId).getRoleName()));

        // 根据角色id删除角色
        roleDao.deleteRole(roleId);

        //删除成功, 返回 "100" "请求成功"
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    @Override
    public ServiceVO setMenu(Integer roleId, String menus) {
        // 先删除当前角色的所有菜单
        roleMenuDao.deleteRoleMenuByRoleId(roleId);

        // 再赋予当前角色新的菜单
        String[] menuArray = menus.split(",");

        for(String str : menuArray){

            RoleMenu rm = new RoleMenu();

            rm.setRoleId(roleId);

            rm.setMenuId(Integer.parseInt(str));

            roleMenuDao.save(rm);

        }

        logService.save(new Log(Log.UPDATE_ACTION,"设置"+roleDao.getRoleById(roleId).getRoleName()+"角色的菜单权限"));

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
