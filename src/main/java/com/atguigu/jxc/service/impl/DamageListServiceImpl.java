package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson2.JSONArray;
import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.DamageListVO;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.DamageListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@SuppressWarnings("all")
public class DamageListServiceImpl implements DamageListService {


    @Autowired
    private DamageListDao damageListDao;

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;

    @Autowired
    private UserDao userDao;




    /**
     * 保存报损单 (保存就是新增)
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @Override
    public ServiceVO save(HttpSession session, DamageList damageList, String damageListGoodsStr) {
        /**
         * 把前端传过来的数据分成两部分 新增到两个表里
         * damageList这一部分, 新增到 t_damage_list 数据表
         * damageListGoodsStr这一部分, 新增到t_damage_list_goods 数据表
         *
         * t_damage_list_goods 数据表的数据, damageListGoodsStr这一部分数据很全
         * t_damage_list 数据表的数据, damageList这一部分, 没有user_id
         * 想办法获取user_id
         */
        //把JSON字符串 转化成 list集合
        List<DamageListVO> jsonList = JSONArray.parseArray(damageListGoodsStr, DamageListVO.class);
        List<DamageListGoods> damageListGoods = JSONArray.parseArray(damageListGoodsStr, DamageListGoods.class);
        //这个json字符串数组 只有一部分, 下标为0的这一部分
        DamageListGoods damageListGoods1 = damageListGoods.get(0);

        User currentUser = (User) session.getAttribute("currentUser");
        Integer userId = currentUser.getUserId();
        damageList.setUserId(userId);

        damageListDao.insertDamageList(damageList);
        String damageNumber = damageList.getDamageNumber();
        Integer damageListId = damageListDao.getDamageListId(damageNumber);
        damageListGoods1.setDamageListId(damageListId);
        damageListGoodsDao.insertDamageListGoods(damageListGoods1);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    /**
     * 报损单查询
     *
     * @param session
     * @param damageList
     * @return
     */
    @Override
    public Map<String, Object> list(HttpSession session, DamageList damageList, String sTime,String eTime) {
        HashMap<String, Object> map = new HashMap<>();

        List<DamageList> list = damageListDao.list(sTime,eTime);

        for (DamageList damageList1 : list) {
            User currentUser = (User) session.getAttribute("currentUser");
            Integer userId = currentUser.getUserId();
            damageList1.setUserId(userId);
            String trueName = currentUser.getTrueName();
            damageList1.setTrueName(trueName);
        }

        map.put("rows",list);
        return map;
    }


    /**
     * 查询报损单商品信息
     * @param damageListGoods
     * @return
     */
    @Override
    public Map<String, Object> goodsList(DamageListGoods damageListGoods) {
        HashMap<String, Object> map = new HashMap<>();
        //商品报损单id
        Integer damageListId = damageListGoods.getDamageListId();
        List<DamageListGoods> damageListGoods1 = damageListGoodsDao.goodsList(damageListId);
        map.put("rows",damageListGoods1);
        return map;
    }
}
