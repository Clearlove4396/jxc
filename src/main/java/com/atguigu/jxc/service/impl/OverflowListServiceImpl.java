package com.atguigu.jxc.service.impl;


import com.alibaba.fastjson2.JSONArray;
import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@SuppressWarnings("all")
public class OverflowListServiceImpl implements OverflowListService {

    @Autowired
    private OverflowListDao overflowListDao;

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;

    /**
     * 分别把信息把存到两个数据表
     * overflowList -> t_overflow_list数据表
     * overflowListGoodsStr -> t_overflow_list_goods数据表
     *
     * overflowList的数据, 差一个userId
     * overflowListGoodsStr的数据, 差一个overflowListId
     * overflowListGoodsStr是字符串数组, 需要用fastjson转化成list列表
     *
     * userId -> 从session中获取 -> session全局会话, 登录后 userId 和 passWord存在session中 ,全局可用
     * overflowListId -> 在 t_overflow_list数据表中 新增信息后, 主键自增, 根据商品报溢单号, 获取overflowListId
     *
     * @param session
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */

    @Override
    public ServiceVO save(HttpSession session, OverflowList overflowList, String overflowListGoodsStr) {
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        overflowList.setUserId(userId);
        overflowListDao.insertOverflowList(overflowList);
        //将JSON字符串转化为list列表
        List<OverflowListGoods> overflowListGoods = JSONArray.parseArray(overflowListGoodsStr, OverflowListGoods.class);
        //取list列表中, 下表为0的部分
        OverflowListGoods overflowListGoods1 = overflowListGoods.get(0);
        String overflowNumber = overflowList.getOverflowNumber();
        Integer overflowListId = overflowListDao.getOverflowListId(overflowNumber);
        overflowListGoods1.setOverflowListId(overflowListId);
        overflowListGoodsDao.insertOverflowListGoods(overflowListGoods1);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    /**
     * 报溢单查询
     * @param session
     * @param overflowList
     * @return
     */
    @Override
    public Map<String, Object> list(HttpSession session, OverflowList overflowList,String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();

        Integer overflowListId = overflowList.getOverflowListId();
        List<OverflowList> list = overflowListDao.list(sTime,eTime);

        for (OverflowList overflowList1 : list) {
            User user = (User) session.getAttribute("currentUser");
            Integer userId = user.getUserId();
            overflowList.setUserId(userId);
            String trueName = user.getTrueName();
            overflowList1.setTrueName(trueName);
        }

        map.put("rows",list);

        return map;
    }




    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        HashMap<String, Object> map = new HashMap<>();

        List<OverflowListGoods> overflowListGoods = overflowListGoodsDao.goodsList(overflowListId);

        map.put("rows",overflowListGoods);

        return map;
    }
}
