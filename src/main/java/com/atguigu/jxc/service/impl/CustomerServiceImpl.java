package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Role;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@SuppressWarnings("all")
public class CustomerServiceImpl implements CustomerService {


    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private LogService logService;


    /**
     * 分页查询客户列表 [客户列表分页]
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();

        // 根据 客户名称 模糊查询 客户列表中客户的数量
        int total = customerDao.getCustomerCount(customerName);

        page = page == 0 ? 1 : page;
        //第几页
        int offSet = (page - 1) * rows;

        // 根据 客户名称 模糊分页查询客户列表  rows -> 每页有几个
        List<Customer> roles = customerDao.getCustomerList(offSet, rows, customerName);

        logService.save(new Log(Log.SELECT_ACTION, "分页查询客户信息"));

        map.put("total", total);
        map.put("rows", roles);

        return map;
    }


    /**
     * 新增或删除客户
     * @param customer
     * @return
     */
    @Override
    public ServiceVO save(Customer customer) {
        // 客户ID为空时，说明是新增操作，需要先判断 角色名 是否存在
        if(customer.getCustomerId() == null) {

            //根据客户名称查询客户角色
            Customer exCustomer = customerDao.findCustomerByName(customer.getCustomerName());

            //判断角色是否存在
            if(exCustomer != null) {

                //不为null, 角色存在, 返回10010, "角色已存在".
                return new ServiceVO<>(ErrorCode.ROLE_EXIST_CODE, ErrorCode.ROLE_EXIST_MESS);

            }

            //反之, 为null, 角色不存在, 执行新增操作
            //日志报告记录
            logService.save(new Log(Log.INSERT_ACTION, "新增客户:" + customer.getCustomerName()));
            //dao层, 操作数据库, 执行新增操作
            customerDao.insertCustomer(customer);

        } else {

            //角色ID不为null, 角色存在, 执行修改操作
            //日志报告记录
            logService.save(new Log(Log.UPDATE_ACTION, "修改角色:"+ customer.getCustomerName()));
            //dao层, 数据库操作, 执行修改操作
            customerDao.updateCustomer(customer);

        }
        //修改成功, 返回 "100", "请求成功"
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }




    /**
     * 删除客户
     * @param ids
     * @return
     */
    @Override
    public ServiceVO delete(String ids) {
        // roleDao.getRoleById(roleId).getRoleName() -> 根据角色id查询角色, 再获取角色名称
        logService.save(new Log(Log.DELETE_ACTION,"删除客户:" + customerDao.getCustomerById(ids).getCustomerName()));

        // 根据角色id删除角色
        customerDao.deleteCustomer(ids);

        //删除成功, 返回 "100" "请求成功"
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }
}
