package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @description
 */
@Service
@SuppressWarnings("all")
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private LogService logService;
    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public  ArrayList<Object> loadGoodsType() {
        logService.save(new Log(Log.SELECT_ACTION, "查询商品类别信息"));

        return this.getAllGoodsType(-1); // 根节点默认从-1开始
    }

    /**
     * 递归查询所有商品类别
     *
     * @return
     */
    public ArrayList<Object> getAllGoodsType(Integer parentId){

        ArrayList<Object> array = this.getGoodSTypeByParentId(parentId);

        for(int i = 0;i < array.size();i++){

            HashMap obj = (HashMap) array.get(i);

            if(obj.get("state").equals("open")){// 如果是叶子节点，不再递归

            }else{// 如果是根节点，继续递归查询
                obj.put("children", this.getAllGoodsType(Integer.parseInt(obj.get("id").toString())));
            }

        }

        return array;
    }

    /**
     * 根据父ID获取所有子商品类别
     * @return
     */
    public ArrayList<Object> getGoodSTypeByParentId(Integer parentId){

        ArrayList<Object> array = new ArrayList<>();

        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(parentId);

        System.out.println("goodsTypeList" + goodsTypeList);
        //遍历商品类别
        for(GoodsType goodsType : goodsTypeList){

            HashMap obj = new HashMap<String, Object>();

            obj.put("id", goodsType.getGoodsTypeId());
            obj.put("text", goodsType.getGoodsTypeName());

            if(goodsType.getGoodsTypeState() == 1){
                obj.put("state", "closed");

            }else{
                obj.put("state", "open");
            }

            obj.put("iconCls", "goods-type");

            HashMap<String, Object> attributes = new HashMap<>();
            attributes.put("state", goodsType.getGoodsTypeState());
            obj.put("attributes", attributes);

            array.add(obj);
        }
        return array;
    }


    /**
     * 新增分类
     * @param goodsTypeName
     * @param pId
     * @return
     */
    @Override
    public ServiceVO save(GoodsType goodsType) {
        /**
         * 新增商品
         * 分类状态为0
         * 获取当前分类的父商品类别ID
         * 判断当前分类有没有父级分类 [有没有goods_type_id 等于 p_id]
         * 如果有父级分类,
         * 判断父级分类下把父级分类的状态变为1
         */
        //修改分类的状态
        goodsType.setGoodsTypeState(0);
        //新增分类
        goodsTypeDao.insertGoodsType(goodsType);
        //获取当前分类的父商品类别ID p_id
        Integer pId = goodsType.getPId();
        //根据 父商品类别ID 获取 父级分类
        GoodsType fatherGoodsType = goodsTypeDao.getFatherGoodsTypeByPId(pId);
        //获取父级分类的商品分类ID
        Integer fatherGoodsTypeId = fatherGoodsType.getGoodsTypeId();
        //判断
        if(fatherGoodsTypeId == pId){//父类商品存在
            //修改父类商品的状态
            fatherGoodsType.setGoodsTypeState(1);
            //把父类商品的数据更新到数据库
            goodsTypeDao.updateGoodsTypeState(fatherGoodsType);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    /**
     * 删除分类
     * @param goodsType
     * @return
     */
    @Override
    public ServiceVO delete(GoodsType goodsType) {
        /**
         * 前端传来一个goodsTypeId
         * 先获取这个数据
         * 然后根据这个数据,从数据库中查询出当前分类的所有数据
         * 获取当前分类的父商品类别id p_id
         * 根据 当前分类的父商品类别ID 查询出所有相同的下属分类 查出一个查一个List<GoodsType>
         * 这个list集合中元素的个数, 就是父级分类的下属分类的个数
         * 通过stream流的方式获取list集合元素的个数count
         * 根据goodsTypeId删除当前分类
         * 然后count-1
         * 即父级分类的下属分类减少一个
         * 判断父级分类中是否还有下属分类,
         * 即count是否等于0
         * 等于0就是没有下属分类
         * 不等于0就是有下属分类,
         * 如果父级分类没有下属分类
         * 就要改变父级分类的状态[ 0 -> 1 ]
         * 有下属分类
         * 父级分类的状态不变, 还是1
         *
         *
         * 如何判断父级分类是否还有下属分类
         * 查一个List<GoodsType>, 判断list集合的长度
         * 如果list长度为0, 就是没有下属分类, 当前分类的goods_type_state就要改为0
         * 如果list长度为1, 就是有下属分类, 当前分类的goods_type_state还是1
         */
        //查询当前分类的所有信息
        //获取商品类别ID
        Integer goodsTypeId = goodsType.getGoodsTypeId();

    //判断当前分类的父级分类是否还有下属分类
        //查询当前分类的所有数据
        GoodsType goodsType1 = goodsTypeDao.find(goodsTypeId);
        //获取当前分类的父商品类别ID
        Integer pId = goodsType1.getPId();
        //根据 当前分类的父商品类别ID 查询出所有相同的下属分类
        List<GoodsType> goodsByPIdList = goodsTypeDao.findByPId(pId);
        //.stream().count() -> 用stream流的方式获取list集合中元素的个数
        long count = goodsByPIdList.stream().count();
        //获取list集合的长度
        //int size = goodsByPIdList.size();
        //根据当前分类的pid 查询当前分类的父级分类的数据
        GoodsType fGT = goodsTypeDao.findList(pId);

        //根据商品类别ID删除当前分类
        goodsTypeDao.delete(goodsTypeId);
        //集合元素-1
        count=count-1;

        //判断集合中是否有元素, 即判断
        if(count == 0){//没有下级元素, 把状态修改为0
            fGT.setGoodsTypeState(0);
        }else { //有下级元素, 状态依然为1
            fGT.setGoodsTypeState(1);
        }

        //把父级分类的数据更新到数据库
        goodsTypeDao.updateGoodsTypeState(fGT);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
