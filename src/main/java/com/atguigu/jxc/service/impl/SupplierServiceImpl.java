package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("all")
@Service
public class SupplierServiceImpl implements SupplierService {


    @Autowired
    private SupplierDao supplierDao;

    @Autowired
    private LogService logService;



    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> getSupplierPage(Integer page, Integer rows, String supplierName) {
        Map<String, Object> map = new HashMap<>();

        // 根据供应商名称模糊查询供应商列表的数量
        int total = supplierDao.getGoodsCount(supplierName);

        //rows:一页存多少数据
        //page:表示第几页
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        // 根据角色名称模糊分页查询角色列表 [多个map集合, 用list列表接收]
        List<Supplier> supplierPage = supplierDao.getSupplierList(offSet,rows,supplierName);

        map.put("total",total);
        map.put("rows",supplierPage);

        return map;
    }


    /**
     * 添加或者修改供应商信息
     *
     * @param supplierId
     * @param supplier
     * @return
     */
    @Override
    public ServiceVO save(Integer supplierId, Supplier supplier) {
        // 供应商Id为空,并且判断供应商名称为null才执行新增操作
        //1.供应商Id为空, 判断供应商名称是否为null
        if(supplierId == null){

            String supplierName = supplier.getSupplierName();

            //根据角色名称查询角色的所有信息
            Supplier supplier1 = supplierDao.findSupplierByName(supplierName);

            if(supplier1 != null){
               return new ServiceVO<>(ErrorCode.ACCOUNT_EXIST_CODE,ErrorCode.ACCOUNT_EXIST_MESS);
            }

            //执行新增操作
            //日志报告记录
            logService.save(new Log(Log.INSERT_ACTION,"新增供应商" + supplierName));
            //新增操作
            supplierDao.insertSupplier(supplier);

        }else {
            //供应商id存在, 执行修改操作
            //日志报告记录
            logService.save(new Log(Log.UPDATE_ACTION, "修改供应商:"+ supplier.getSupplierName()));
            //dao层, 数据库操作, 执行修改操作
            supplierDao.updateSupplier(supplier);
        }
        //修改成功, 返回 "100", "请求成功"
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    /**
     * 删除供应商
     * @param ids 就是supplierId
     * @return
     */
    @Override
    public ServiceVO delete(String ids) {

        //根据供应商编号id获取供应商, Supplier类型
        Supplier supplierById = supplierDao.getSupplierById(ids);

        //supplierById.getSupplierName() 获取供应商名称
        logService.save(new Log(Log.DELETE_ACTION,"删除供应商" + supplierById.getSupplierName()));

        //根据供应商编号id删除供应商
        supplierDao.deleteSupplier(ids);

        //删除成功, 返回"100" "请求成功"
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }
}
