package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerReturnListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;

import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.dao.SaleListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @description
 */
@SuppressWarnings("all")
@Slf4j
@Service
public class GoodsServiceImpl implements GoodsService  {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private LogService logService;

    @Autowired
    private SaleListGoodsDao saleListGoodsDao;

    @Autowired
    private CustomerReturnListGoodsDao customerReturnListGoodsDao;

    @Autowired
    private GoodsTypeDao goodsTypeDao;



    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }


    /**
     * 分页查询首页商品
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsPage(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();

        int total = goodsDao.getGoodsCount(codeOrName,goodsTypeId);

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goodsPage = goodsDao.getGoodsPage(offSet, rows, codeOrName, goodsTypeId);

        goodsPage.forEach(goods->{
            //获取商品编号id
            Integer goodsId = goods.getGoodsId();
            //获取商品类别id
            Integer goodsTypeId1 = goods.getGoodsTypeId();

            //获取销售总量
            Integer saleTotal = saleListGoodsDao.getSaleTotalByGoodsId(goodsId);

            if(saleTotal==null){
                goods.setSaleTotal(0);
            }

            //获取退货总量
            Integer customerReturnTotal
                    = customerReturnListGoodsDao.getCustomerReturnTotalByGoodsId(goodsId);

            if(saleTotal!=null && customerReturnTotal==null){
                goods.setSaleTotal(saleTotal);
            }

            if(saleTotal!=null && customerReturnTotal!=null){
                goods.setSaleTotal(saleTotal-customerReturnTotal);
            }

            //根据 商品类别id 获取
            GoodsType goodsTypeById =  goodsTypeDao.getGoodsTypeById(goodsTypeId1);
            //商品类别名称
            goods.setGoodsTypeName(goodsTypeById.getGoodsTypeName());


        });

        logService.save(new Log(Log.SELECT_ACTION, "分页查询商品信息"));

        map.put("total", total);
        map.put("rows", goodsPage);

        return map ;

    }


    @Override
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();

        int total = goodsDao.getGoodsCount2(goodsName,goodsTypeId);

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goodsPage2 = goodsDao.getGoodsPage2(offSet, rows, goodsName, goodsTypeId);

        //遍历, 把给每一个商品 赋值 商品名称
        for (Goods goods2 : goodsPage2) {
            Integer goodsTypeId2 = goods2.getGoodsTypeId();
            //根据商品类别id 获取 商品分类的所有信息
            GoodsType goodsType = goodsTypeDao.find(goodsTypeId2);
            //获取商品分类名称
            String goodsTypeName = goodsType.getGoodsTypeName();
            //把商品分类名称给商品
            goods2.setGoodsTypeName(goodsTypeName);
        }

        map.put("total",total);
        map.put("rows",goodsPage2);

        return map;
    }


    /**
     * 新增或修改商品
     * @param goods
     * @return
     */
    @Override
    public ServiceVO save(Goods goods) {
        Integer goodsId = goods.getGoodsId();
        if(goodsId==null){
            //新增
            goodsDao.insertGoods(goods);
        }else {
            //修改操作
            goodsDao.updateGoods(goods);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    /**
     * 删除商品
     */
    @Override
    public ServiceVO delete(Goods goods) {
        /**
         * 获取商品编号id goods_id
         * 根据商品编号id 从数据中获取 商品的所有数据
         * 获取商品状态
         * 判断
         * 商品状态为0的可以删除
         * 不为0的, 不删除
         */
        //获取商品编号id goods_id
        Integer goodsId = goods.getGoodsId();
        //根据商品编号id 从数据中获取 商品的所有数据
        Goods goodsBygoodsId = goodsDao.getGoodsBygoodsId(goodsId);
        //获取商品状态
        Integer state = goodsBygoodsId.getState();
        //判断
        if(state == 0){//商品状态为0,可以删除
            //删除操作
            goodsDao.delete(goodsId);
        }else{//为1和2不可以删除
            System.out.println("该商品不可删除");
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    /**
     * 分页查询无库存商品信息
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getNoIQPage(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();

        //rows:一页存多少数据
        //page:表示第几页
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        //分页查询无库存商品
        List<Goods> noIQpage = goodsDao.getNoIQpage(offSet,rows,nameOrCode);

        for (Goods goods : noIQpage) {
            //获取商品编码
            String goodsCode = goods.getGoodsCode();
            //根据商品编码或者商品名称 或的 商品的全部数据
            Goods goodsByNameOrCode = goodsDao.getGoodsByNameOrCode(goodsCode);
            //获取商品类别id
            Integer goodsTypeId = goodsByNameOrCode.getGoodsTypeId();
            //根据商品类别id 从数据库中获取 商品类别的所有数据
            GoodsType goodsType = goodsTypeDao.find(goodsTypeId);
            //获取商品类别名称
            String goodsTypeName = goodsType.getGoodsTypeName();
            //把商品类别名称 赋值给 商品
            goods.setGoodsTypeName(goodsTypeName);
            }
            map.put("rows",noIQpage);
            return map;
    }


    /**
     * 分页查询有库存商品的信息
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getHasIQPage(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();

        //rows:一页存多少数据
        //page:表示第几页
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        //从数据库, 分页查询有库存商品的信息
        List<Goods> hasIQPage = goodsDao.getHasIQPage(offSet,rows,nameOrCode);

        for (Goods goods : hasIQPage) {
            //获取商品编码
            String goodsCode = goods.getGoodsCode();
            //根据商品编码或者商品名称 或的 商品的全部数据
            Goods goodsByNameOrCode = goodsDao.getGoodsByNameOrCode(goodsCode);
            //获取商品类别id
            Integer goodsTypeId = goodsByNameOrCode.getGoodsTypeId();
            //根据商品类别id 从数据库中获取 商品类别的所有数据
            GoodsType goodsType = goodsTypeDao.find(goodsTypeId);
            //获取商品类别名称
            String goodsTypeName = goodsType.getGoodsTypeName();
            //把商品类别名称 赋值给 商品
            goods.setGoodsTypeName(goodsTypeName);
        }


        map.put("rows",hasIQPage);

        return map;
    }


    /**
     * 添加无库存商品, 修改有库存商品的数量或成本价
     * @param goods
     * @return
     */
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        //获取当前商品的所有数据
        Goods goodsBygoodsId = goodsDao.getGoodsBygoodsId(goodsId);
        //无库存商品当前的库存
        Integer inventoryQuantity1 = goodsBygoodsId.getInventoryQuantity();
        //获取当前商品的成本价
        double purchasingPrice1 = goodsBygoodsId.getPurchasingPrice();
        if(inventoryQuantity1 == 0){ //无库存商品,做新增操作
            inventoryQuantity1 = inventoryQuantity;
            purchasingPrice1 = purchasingPrice;
            goodsBygoodsId.setInventoryQuantity(inventoryQuantity1);
            goodsBygoodsId.setPurchasingPrice(purchasingPrice1);
            Integer goodsId1 = goodsBygoodsId.getGoodsId();
            goodsDao.updateGoodsByGoodsId(goodsBygoodsId);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    @Override
    public ServiceVO deleteStock(Goods goods) {
        //获取商品编号id
        Integer goodsId = goods.getGoodsId();
        //通过商品编号id 从数据库中获取 当前商品的全部数据
        Goods goodsBygoodsId = goodsDao.getGoodsBygoodsId(goodsId);
        Integer state = goodsBygoodsId.getState();
        if(state == 0 ){
            goodsBygoodsId.setInventoryQuantity(0);
            goodsDao.updateGoodsByGoodsId(goodsBygoodsId);
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        }
            return new ServiceVO<>(SuccessCode.FAIL_CODE, SuccessCode.FAIL_MESS);
    }


    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * @param goods
     * @return
     */
    @Override
    public Map<String, Object> listAlarm(Goods goods) {
        HashMap<String, Object> map = new HashMap<>();
        List<Goods> goodsList = goodsDao.getGoodsList(goods);
        List<Goods> goodsList2 = new ArrayList<>();
        for (Goods goods1 : goodsList) {
            //库存数量
            Integer inventoryQuantity = goods1.getInventoryQuantity();
            //库存下限
            Integer minNum = goods1.getMinNum();
            if(inventoryQuantity<minNum){
                Integer goodsId = goods1.getGoodsId();
                Goods goodsBygoodsId = goodsDao.getGoodsBygoodsId(goodsId);
                Integer goodsTypeId = goodsBygoodsId.getGoodsTypeId();
                GoodsType goodsTypeById = goodsTypeDao.getGoodsTypeById(goodsTypeId);
                String goodsTypeName = goodsTypeById.getGoodsTypeName();
                goodsBygoodsId.setGoodsTypeName(goodsTypeName);
                goodsList2.add(goodsBygoodsId);
            }
        }
        map.put("rows",goodsList2);
        return map;
    }
}
