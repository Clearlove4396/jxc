package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@SuppressWarnings("all")
public class UnitServiceImpl implements UnitService {

    @Autowired
    private UnitDao unitDao;

    /**
     * 查询所有单位
     * @return
     */
    @Override
    public Map<String, Object> list() {
        Map<String, Object> map = new HashMap<>();

        List<Unit> unitList = unitDao.list();

        map.put("rows",unitList);

        return map;
    }
}
