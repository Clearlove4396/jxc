package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface OverflowListService {

    /**
     * 新增商品报溢单
     *
     * @param session
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    ServiceVO save(HttpSession session, OverflowList overflowList, String overflowListGoodsStr);


    /**
     * 报溢单查询
     * @param session
     * @param overflowList
     * @return
     */
    Map<String, Object> list(HttpSession session, OverflowList overflowList,String sTime, String eTime);


    /**
     * 报溢单商品信息列表查询
     * @param overflowListId
     * @return
     */
    Map<String, Object> goodsList(Integer overflowListId);
}
