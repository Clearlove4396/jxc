package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {


    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    Map<String, Object> getSupplierPage(Integer page, Integer rows, String supplierName);

    /**
     * 添加或修改供应商信息
     *
     * @param supplierId
     * @param supplier
     * @return
     */
    ServiceVO save(Integer supplierId, Supplier supplier);


    /**
     * 删除供应商信息
     * @param ids 就是supplierId
     * @return
     */
    ServiceVO delete(String ids);
}
