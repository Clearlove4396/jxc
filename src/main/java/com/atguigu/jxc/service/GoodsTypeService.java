package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.GoodsType;

import java.util.ArrayList;

/**
 * @description
 */
public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();


    /**
     * 新增分类
     * @param goodsType
     * @return
     */
    ServiceVO save(GoodsType goodsType);


    /**
     * 删除分类
     * @param goodsType
     * @return
     */
    ServiceVO delete(GoodsType goodsType);
}
