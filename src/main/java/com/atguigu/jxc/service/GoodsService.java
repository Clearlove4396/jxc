package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public interface GoodsService  {



    ServiceVO getCode();



    /**
     * 分页查询首页商品
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    Map<String, Object> getGoodsPage(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);




    /**
     * 分页查询所有商品
     */
    Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    /**
     * 添加或更新商品
     * @param goods
     * @return
     */
    ServiceVO save(Goods goods);


    /**
     * 删除商品
     * @param goods
     * @return
     */
    ServiceVO delete(Goods goods);


    /**
     * 分页查询无库存商品的信息
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    Map<String, Object> getNoIQPage(Integer page, Integer rows, String nameOrCode);

    /**
     * 分页查询有库存商品的信息
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    Map<String, Object> getHasIQPage(Integer page, Integer rows, String nameOrCode);

    /**
     * 添加无库存商品, 修改有库存商品的数量或成本价
     * @return
     */
    ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    /**
     * 删除商品库存
     * @param goods
     * @return
     */
    ServiceVO deleteStock(Goods goods);


    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * @param goods
     * @return
     */
    Map<String, Object> listAlarm(Goods goods);
}
