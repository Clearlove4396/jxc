package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface DamageListService {


    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    ServiceVO save(HttpSession session, DamageList damageList, String damageListGoodsStr);


    /**
     * 报损单查询
     * @param session
     * @param damageList
     * @return
     */
    Map<String, Object> list(HttpSession session, DamageList damageList, String sTime,String eTime);

    /**
     * 查询报损单商品信息
     * @param damageListGoods
     * @return
     */
    Map<String, Object> goodsList(DamageListGoods damageListGoods);
}
