package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@SuppressWarnings("all")
@RestController
@RequestMapping("/supplier")
public class SupplierController {


    @Autowired
    private SupplierService supplierService;


    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list(Integer page,Integer rows,String supplierName){
            return supplierService.getSupplierPage(page,rows,supplierName);
    }


    /**
     * 添加或修改供应商信息
     * @param supplierId
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(Integer supplierId, Supplier supplier){
            return supplierService.save(supplierId,supplier);
    }


    /**
     * 删除供应商信息
     * @param ids  其实就是supplierId
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(String ids){
        return supplierService.delete(ids);
    }

}
