package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import javax.xml.ws.Action;
import java.util.Map;

@SuppressWarnings("all")
@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListController {

    @Autowired
    private OverflowListService overflowListService;


    /**
     * 新增商品报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(HttpSession session, OverflowList overflowList, String overflowListGoodsStr){
        return overflowListService.save(session,overflowList,overflowListGoodsStr);
    }


    @PostMapping("/list")
    public Map<String,Object> list(HttpSession session,OverflowList overflowList,String sTime, String eTime){
            return overflowListService.list(session,overflowList,sTime,eTime);
    }


    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(Integer overflowListId){
            return overflowListService.goodsList(overflowListId);
    }

}
