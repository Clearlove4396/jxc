package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.UserLogin;
import com.atguigu.jxc.service.DamageListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListController {

    @Autowired
    private DamageListService damageListService;


    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(HttpSession session, DamageList damageList, String damageListGoodsStr){
        return damageListService.save(session,damageList,damageListGoodsStr);
    }

    /**
     * 报损单查询
     * @param damageList
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list(HttpSession session,
                                   DamageList damageList,
                                   String sTime,
                                   String eTime){
            return damageListService.list(session,damageList,sTime,eTime);
    }


    /**
     * 查询报损单商品信息
     * @param damageListGoods
     * @return
     */
    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(DamageListGoods damageListGoods){
        return damageListService.goodsList(damageListGoods);
    }

}
