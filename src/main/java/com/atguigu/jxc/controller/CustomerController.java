package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@SuppressWarnings("all")
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 分页查询客户信息 [客户列表分页]
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list(Integer page, Integer rows, String customerName){
            return customerService.list(page,rows,customerName);
    }


    /**
     * 新增或删除客户
     * @param customer
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(Customer customer){
            return customerService.save(customer);
    }


    /**
     * 删除客户
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO save(String ids){
        return customerService.delete(ids);
    }

}
