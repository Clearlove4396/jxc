package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @description 商品信息Controller
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private UserService userService;

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/listInventory")
    public Map<String, Object> page(Integer page, Integer rows, String codeOrName, Integer goodsTypeId){
       return goodsService.getGoodsPage(page,rows,codeOrName,goodsTypeId);
    }


    /**
     * 分页查询所有商品
     */
    @PostMapping("/list")
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId){
        return goodsService.list(page,rows,goodsName,goodsTypeId);
    }


    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }


    /**
     * 添加或更新商品
     * @param goods
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(Goods goods){
        return goodsService.save(goods);
    }

    /**
     * 删除商品信息
     */
    @PostMapping("/delete")
    public ServiceVO delete(Goods goods){
            return goodsService.delete(goods);
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode){
       return goodsService.getNoIQPage(page,rows,nameOrCode);
    }


    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode){
            return goodsService.getHasIQPage(page,rows,nameOrCode);
    }


    /**
     * 添加无库存商品, 修改有库存商品的数量或成本价
     */
    @PostMapping("/saveStock")
    public ServiceVO saveStock(Integer goodsId,Integer inventoryQuantity,double purchasingPrice){
        return goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
    }


    /**
     * 删除商品库存
     */
    @PostMapping("/deleteStock")
    public ServiceVO deleteStock(Goods goods){
            return goodsService.deleteStock(goods);
    }

    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    @PostMapping("/listAlarm")
    public Map<String, Object> listAlarm(Goods goods){
        return goodsService.listAlarm(goods);
    }
}
