package com.atguigu.jxc.entity;

import lombok.Data;
/**
 * 供应商
 */
@Data
public class Supplier {

  private Integer supplierId;  //供应商id | 供应商编号
  private String supplierName; //供应商名称
  private String contacts;  //联系人
  private String phoneNumber;  //联系人电话
  private String address;  //供应商地址
  private String remarks;  //备注

}
