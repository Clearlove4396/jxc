package com.atguigu.jxc.entity;

import lombok.Data;
/**
 * 报溢
 */
@Data
public class OverflowList {

  private Integer overflowListId;   //商品报溢单id
  private String overflowNumber;  //商品报溢单号
  private String overflowDate;  //报溢日期
  private String remarks;  //备注
  private Integer userId; //用户id

  private String trueName;

}
