package com.atguigu.jxc.entity;

import lombok.Data;
/**
 * 商品信息实体
 */
@Data
public class Goods {

  private Integer goodsId; //商品编号ID
  private String goodsCode; //商品编码
  private String goodsName; //商品名称
  private Integer inventoryQuantity;
  private double lastPurchasingPrice;
  private Integer minNum;
  private String goodsModel; //商品型号
  private String goodsProducer;
  private double purchasingPrice;
  private String remarks;
  private double sellingPrice;
  private Integer state;// 0 初始化状态 1 期初库存入仓库  2  有进货或者销售单据
  private String goodsUnit;
  private Integer goodsTypeId;  //商品类别ID

  private String goodsTypeName; //商品类别名称

  private Integer saleTotal;// 销售总量

}
