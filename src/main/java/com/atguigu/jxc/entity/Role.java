package com.atguigu.jxc.entity;

import lombok.Data;
/**
 * 角色
 */
@Data
public class Role {

  private Integer roleId;  //角色Id
  private String roleName;  //角色名称
  private String remarks;  //备注

}
