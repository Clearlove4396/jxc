package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();




    /**
     * 分页查询首页商品
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    List<Goods> getGoodsPage(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    int getGoodsCount(@Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    int getGoodsCount2(@Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    List<Goods> getGoodsPage2(@Param("offSet") Integer offSet,@Param("rows") Integer rows, String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 添加或新增商品
     * @param goods
     * @return
     */
    Integer insertGoods(Goods goods);

    //修改操作
    Integer updateGoods(Goods goods);

    //根据商品编号id 从数据中获取 商品的所有数据
    Goods getGoodsBygoodsId(@Param("goodsId") Integer goodsId);

    //删除操作
    Integer delete(Integer goodsId);


    //分页查询无库存的商品
    List<Goods> getNoIQpage(int offSet, Integer rows, @Param("nameOrCode") String nameOrCode);

    //根据商品编码或者商品名称 或的 商品的全部数据
    Goods getGoodsByNameOrCode(@Param("goodsCode") String goodsCode);

    //分页查询有库存的商品
    List<Goods> getHasIQPage(int offSet, Integer rows, String nameOrCode);


    //通过商品编号更新
    Integer updateGoodsByGoodsId(Goods goodsBygoodsId);

    //查询所有商品的列表
    List<Goods> getGoodsList(Goods goods);
}
