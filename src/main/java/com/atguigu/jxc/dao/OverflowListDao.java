package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowListDao {

    //新增商品报溢单
    Integer insertOverflowList(OverflowList overflowList);

    //根据 商品报溢单号 获取 商品报溢单id
    Integer getOverflowListId(String overflowNumber);

    //查询报溢单
    List<OverflowList> list(@Param("overflowDate1") String sTime, @Param("overflowDate2") String eTime);
}
