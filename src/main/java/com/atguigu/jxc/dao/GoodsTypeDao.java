package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    GoodsType getGoodsTypeById(@Param("goodsTypeId1") Integer goodsTypeId1);


    /**
     * 新增分类
     * @param goodsType
     * @return
     */
    Integer insertGoodsType(GoodsType goodsType);

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    Integer delete(Integer goodsTypeId);

    /**
     * 根据goodsTypeId查询当前分类的所有信息
     * @param goodsTypeId
     */
    GoodsType find(@Param("goodsTypeId") Integer goodsTypeId);


    //根据当前分类的pid 查询当前分类的父级分类 [父级分类的goodsTypeId 等于 当前分类的pId]
    GoodsType findList(@Param("goodsTypeId") Integer pId);


    //根据 当前分类的父商品类别ID 查询出所有相同的下属分类
    List<GoodsType> findByPId(Integer pId);

    //根据 父商品类别ID 获取 父级分类
    GoodsType getFatherGoodsTypeByPId(@Param("goodsTypeId") Integer pId);


}
