package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListDao {


    //新增商品报损单
    Integer insertDamageList(DamageList damageList);

    //根据 商品报损单号 获取商品报损单ID
    Integer getDamageListId(String damageNumber);

    //查询 报损单列表
    List<DamageList> list(@Param("damageDate1") String sTime,@Param("damageDate2") String eTime);
}
