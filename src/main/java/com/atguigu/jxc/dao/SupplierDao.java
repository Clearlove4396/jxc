package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {

    // 根据供应商名称模糊查询供应商列表的数量
    int getGoodsCount(@Param("supplierName") String supplierName);

    // 根据供应商名称模糊分页查询供应商列表
    List<Supplier> getSupplierList(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("supplierName") String supplierName);


    //根据角色名称查询角色的所有信息
    Supplier findSupplierByName(@Param("supplierName") String supplierName);


    //新增供应商
    void insertSupplier(Supplier supplier);

    //修改供应商
    void updateSupplier(Supplier supplier);

    //根据供应商编号id获取供应商
    Supplier getSupplierById(@Param("supplierId") String ids);

    //根据供应商编号id删除供应商
    Integer deleteSupplier(@Param("supplierId") String ids);
}
