package com.atguigu.jxc.dao;


import com.atguigu.jxc.domain.DamageListVO;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListGoodsDao {

    //新增 商品报损单商品列表
    Integer insertDamageListGoods(DamageListGoods damageListGoods1);

    //根据商品报损单id 查询 报损单商品信息
    List<DamageListGoods> goodsList(@Param("damageListId") Integer damageListId);
}
