package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowListGoodsDao {

    /**
     * 新增商品报溢单
     * @param overflowListGoods1
     * @return
     */
    Integer insertOverflowListGoods(OverflowListGoods overflowListGoods1);


    /**
     * 查询报溢单商品的列表
     * @param overflowListId
     * @return
     */
    List<OverflowListGoods> goodsList(@Param("overflowListId") Integer overflowListId);
}
