package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Role;
import org.apache.ibatis.annotations.Param;

import javax.management.relation.RelationNotification;
import java.util.List;

public interface CustomerDao {

    // 根据 客户名称 模糊查询 客户列表中客户的数量
    Integer getCustomerCount(@Param("customerName") String customerName);


    // 根据 客户名称 模糊分页查询客户列表
    List<Customer> getCustomerList(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("customerName") String customerName);


    //根据客户名称查询客户
    Customer findCustomerByName(@Param("customerName") String customerName);

    //新增客户
    Integer insertCustomer(Customer customer);

    //更新客户
    Integer updateCustomer(Customer customer);


    Customer getCustomerById(@Param("customerId") String ids);

    Integer deleteCustomer(@Param("customerId") String ids);
}
