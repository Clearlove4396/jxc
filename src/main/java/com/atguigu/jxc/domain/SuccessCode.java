package com.atguigu.jxc.domain;

/**
 * @description 成功码
 */
public interface SuccessCode {

    int SUCCESS_CODE = 100;
    String SUCCESS_MESS = "请求成功";

    int FAIL_CODE = 200;
    String FAIL_MESS = "请求失败";

}
