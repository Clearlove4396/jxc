package com.atguigu.jxc.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class DamageListVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer goodsId;
    private Integer goodsTypeId;
    private String goodsCode;
    private String goodsName;
    private String goodsModel;
    private String goodsUnit;
    private double lastPurchasingPrice;
    private double price;
    private Integer goodsNum;
    private double total;

}
